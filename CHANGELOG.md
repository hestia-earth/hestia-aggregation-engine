# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.20.13](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.20.12...v0.20.13) (2025-03-04)


### Features

* **queries:** use unverified aggregation in global `processedFood` ([809bf92](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/809bf924e132e7e6969e6c4f94c54fd7aa016ec3))
* remove empty fields from aggregated data ([fd2e47b](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/fd2e47bd4384ed3719dec688ee55bf761401bcf1))


### Bug Fixes

* **aggregate:** fetch original Site for global aggregations ([2a62891](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/2a6289110595fb032a65a3c9f1baf157d5c5fe16))
* **aggregation:** use `value` as potential `min` / `max` ([d4eb7e1](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/d4eb7e12ca6d2610645bf578b125c202019a0ecf))
* **queries:** always match primary product for global aggregation ([b2b5a72](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/b2b5a722fb73e0d86e2f3d0d7236ec07c276bded))
* **queries:** fix date range search ([6affb89](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/6affb89b56d581164fc320b8b69cafbe8a04d060))
* **queries:** fix error couting global nodes ([c160474](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/c1604748ffd14ef511aed66ed5834e60554994a3))
* **queries:** fix incorrect query on sub-region nodes ([8ee9005](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/8ee9005a82d821e482c0f195a49665fd669df423))
* **queries:** remove exception on global aggregations needs verified country ([43cd64a](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/43cd64a77308f8dd53d22fed40cf9741cfb36705))

### [0.20.12](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.20.11...v0.20.12) (2025-02-26)


### Features

* enable aggregation of non primary products for `processedFood` ([dd2a66d](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/dd2a66db8bc1ee3e8cf3ba6e35d11bbc68694ee5))


### Bug Fixes

* **queries:** fix error fetching Cycles outside the date range ([4093d0f](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/4093d0f8f2fdc530c1d19859f7fd91e3e35361ba))

### [0.20.11](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.20.10...v0.20.11) (2025-02-26)


### Features

* **blank-nodes:** remove blank nodes where type in `skipAggregatedNodeTypes` ([50831bd](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/50831bdb780877c31d59a52b06b349f87cf677cc))

### [0.20.10](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.20.9...v0.20.10) (2025-02-18)


### Features

* aggregate `processedFood` on `agri-food processor` ([4de2074](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/4de207454e6ed447daba18fe13d7bce1412d0e8c))
* **emissions:** handle `skipAggregation` lookup ([86383e8](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/86383e8d8514bee57c44b28173c42060aecd10ed))
* **management:** aggregate only in current period and 20years prior ([6a00b98](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/6a00b9888845d5cfbeef355d876f12582741b6d4))
* **quality-score:** ignore Emissions that are skipped for aggregation ([2e5127d](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/2e5127dff15a44f24820de11c8503e7ff71daebc))
* **queries:** restrict by `siteType` for country nodes ([0ee2ced](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/0ee2ced6a4e6ebd32b6c9eae2e5ff4a8813f959b))
* **queries:** restrict to `aggregatedDataValidated` for global aggregations ([b7bf72b](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/b7bf72b624bb5c0f0318b685a1ad18586cddf143))
* remove `% area` blank nodes with `0` value when summing to 100% ([763d7b1](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/763d7b11949fa2c9935e6d94520e515675a9f81d))
* **site:** aggregate `management` ([39eafe0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/39eafe05b978e5ad495ac25abd7cbb5994717327))
* **terms:** rescale products if using `dryMatter` ([e281698](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/e28169896d911520c60f6ae85f7aaaabe1ca250e))


### Bug Fixes

* **blank-node:** fix grouping of management nodes by date ([2396ab9](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/2396ab9922cfcc854ae7696ff54374553ce9e904))
* **management:** update Management dates so they dont overlap with date period ([8532f27](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/8532f278e268d4442df7bec43dcd50b94a59caa9))
* **queries:** aggregate more than 10,000 Cycles ([b1a49a0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/b1a49a0bd877ef5fbe4dbf393d728d8e2b1e5907))
* **site:** group `Management` by `startDate` and `endDate` ([34278d9](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/34278d960a6f3ccb1ecbfd00449bc1c5741e779a))

### [0.20.9](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.20.8...v0.20.9) (2025-02-04)


### Features

* **cycle:** raise error when Site can not be downloaded ([52a1830](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/52a1830af9d2885535858386d6c85cfa2b92fd6f))
* **queries:** improve default error message failed to download node ([b7f624a](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/b7f624ad44b763792e3d66883066a86edb61be10))
* **queries:** throw error for all nodes without recalculated data ([2031d0f](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/2031d0f202f23f28cd49ee32edcd5aba69df2b84))


### Bug Fixes

* **blank-node:** handle boolean values for missing data ([bbb90b8](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/bbb90b87d93dc89ff744b22956bb9ef7359515e1))

### [0.20.8](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.20.7...v0.20.8) (2025-01-20)


### Features

* aggregate measurements without depths ([3025efd](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/3025efdb8453ab2814a7c5ec2088f42225471e90))
* **blank-node:** add 0 for all blank node in the same group equal 100 ([fb1fa3e](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/fb1fa3ee66dee302abdab32a13344c96b9fe2145)), closes [#137](https://gitlab.com/hestia-earth/hestia-aggregation-engine/issues/137)
* **practice:** aggregate all practices ([cecf1e3](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/cecf1e39e18df3850da25e9c589b79933576978f))


### Bug Fixes

* **practices:** ignore completeness on blank nodes without completeness mapping ([540a17b](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/540a17b782662129193e9a96630510b3ed138628))
* **terms:** include incomplete products with value 0 ([425adc3](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/425adc3e6be44ee26a996400261e931ed5711d32))

### [0.20.7](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.20.6...v0.20.7) (2024-12-10)


### Bug Fixes

* **quality-score:** get FAO yield in kg/ha from lookup ([a367110](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/a367110fc0ab07a30c4c28bdc6400b3ca1507b22))

### [0.20.6](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.20.5...v0.20.6) (2024-11-12)


### Features

* **terms:** account for incomplete primary product `value` ([0592fa3](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/0592fa36e69255063d8144a16683b00f1e187162))


### Bug Fixes

* **cycle:** handle all products are incomplete skip aggregation ([22bb9f2](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/22bb9f2d314fb6dd314be2a6ecfb923b849404a0))
* **cycle:** handle no product value normalize for non-crop product ([e29a903](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/e29a903ee7e2459a6cfdf069dfa0b002b9a18c22))
* **terms:** account for blank node without `min` and `max` values ([955ddc8](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/955ddc835fea2a906f1312e9aeb5d058fe8e1a79))
* **terms:** fix count missing products ([ef57b96](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/ef57b96a5a6730a90d102b6a97a93db59d3c5715))

### [0.20.5](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.20.4...v0.20.5) (2024-10-29)


### Features

* **cycle:** aggregate with no product `value` or `economicValueShare` ([6132488](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/61324884f6ee39c8b52db7c06c410f4ea3c2858b))

### [0.20.4](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.20.3...v0.20.4) (2024-10-14)


### Features

* **cycle:** skip where `commercialPracticeTreatment` is `false` ([0f4848f](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/0f4848ff99780441b827dfabf993647c36be7715))
* **terms:** ignore values when completeness is `False` ([862d59b](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/862d59b00c040486790440efe4954b8c1448c969))


### Bug Fixes

* **site:** propagate min, max and observations on country level ([5d09191](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/5d09191fd8d827c06c73cdd3a49bd6e3ee8f04fc))

### [0.20.3](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.20.2...v0.20.3) (2024-08-19)


### Features

* **site:** aggregate `numberOfSites` ([a11051e](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/a11051e15a3b46528f3bc1f2c4d91483a7bb29ec))

### [0.20.2](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.20.1...v0.20.2) (2024-08-06)


### Features

* **blank node:** handle boolean values and `booleanGroup` lookup ([96cb261](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/96cb261e7f8987fcc54c35255b652bb6724bab5d))


### Bug Fixes

* **cycle:** filter blank nodes by `dates` instead of grouping ([08ed5cc](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/08ed5cc9f027b48dcdf2d9231b950f8165ce9869))
* **cycle:** skip Cycle when Site is not fully recalculated ([3f7d1f8](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/3f7d1f8bb32e12a52e0459766f162f09069e7f0d))

### [0.20.1](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.20.0...v0.20.1) (2024-07-23)


### Features

* **queries:** add function to count the number of nodes to aggregate ([33f0d5a](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/33f0d5a64fce3dd10848d2b1233f6dadc454acc1))


### Bug Fixes

* **cycle:** remove `min`, `max`, and `sd` from aggregated blank nodes ([f29f1c2](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/f29f1c2ea22633d6a8333e132b8e75ff4c1d51b9))
* **queries:** fix wrong query for global aggregation ([2d6245a](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/2d6245a27991ff725b0cd489c2d41de1801d4164))
* **queries:** skip recalculate node that have not reached their last stage ([ddd148e](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/ddd148ec7793bb09d4668cb8ae9809626540b5b7))

## [0.20.0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.19.2...v0.20.0) (2024-07-09)


### ⚠ BREAKING CHANGES

* **site:** Min schema version required is `28`.

### Bug Fixes

* **site:** aggregate `Measurement` by `dates` ([dccfbb7](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/dccfbb77187df58497438e7715cdd2f28aa19089))
* **site:** force `depthUpper` and `depthLower` as integers ([dab25a4](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/dab25a406ace10ce1e7199a504f26c86e7eee7e4))

### [0.19.2](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.19.1...v0.19.2) (2024-04-30)


### Features

* **blank node:** handle group for 100% sum ([79eea30](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/79eea3090e66b0db471274a4f7764b19241265ac))
* **cycle:** use `irrigated` lookup to detect irrigated practices ([87b4832](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/87b48327e9f59df19646b67f887802f4f88578ff))

### [0.19.1](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.19.0...v0.19.1) (2024-04-02)


### Features

* **cycle:** do not auto-generate ImpactAssessment ([9f09768](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/9f09768d1ef5085ad518b9c46417f7f6c525b7ee))


### Bug Fixes

* **countries:** account for missing co-products ([4cc3f88](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/4cc3f886dfbacfac059d77334a910b8fe976d002))
* **cycle product:** aggregate `value` and `economicValueShare` using weights ([e7f666e](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/e7f666e505ac370dff4c015ba09364352a3da3b3))
* **terms:** account for missing co-products ([40e79ee](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/40e79ee5d1840e85013c92c9225722d05d6948bc))
* **world:** account for missing co-products ([73d4896](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/73d4896b0c9c31b8e66266e766d4ecfb0da2c162))

## [0.19.0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.18.1...v0.19.0) (2024-03-19)


### ⚠ BREAKING CHANGES

* Min schema version is `27.1.0`.

### Features

* **cycle:** add timestamp to generated ID ([46ef718](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/46ef71808d138881eb0c2c5f5cf1d1f6bf490fbd))
* **practice:** aggregate `landUseManagement` in `days` ([1a2ba45](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/1a2ba45f3fcd1b378025b96a5e8d7b28cfdf4a8b))
* set `aggregatedDataValidated=false` on all aggregated nodes ([09a1e3d](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/09a1e3dd696cc3e6aff67405052d32204705f473))
* **site measurements:** handle `skipAggregation` ([aeac5f4](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/aeac5f41389dcdd583788168ec97a2f15df04345))

### [0.18.1](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.18.0...v0.18.1) (2024-03-04)


### Features

* aggregate every 20 years ([754cc55](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/754cc55f1dc18be0dbd1fd219ef411cd69e9c9e0))
* **cycle:** skip organic/irrigated matrix on non-crop products ([829fc5d](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/829fc5dc15c13bb8acdfc231dca8394a96eb3d60))
* **impact assessment:** link to `site` ([5d3946d](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/5d3946d7c53fa6f7f93258b10ed53a495b277de1))
* **quality score:** log incomplete keys ([de573a5](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/de573a54c72cfe37e8ab12e97b05261a620efbe0))


### Bug Fixes

* **cycle:** fix average of primary product quantity ([bfcec4c](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/bfcec4c9329cb2a410089b6aa247e2de9de0fd73))
* **cycle:** use all `irrigated` practices to detect irrigated Cycle ([6636191](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/6636191bc6a3d006ad58f2eb1ebf43b305878410))
* **quality score:** fix error when not filtering by score ([efca1c3](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/efca1c37089c5d3552e936c5fbb23f62b92e0a16))
* **quality score:** handle no yield in FAOSTAT ([2148679](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/21486797e2b8d8af63611738cff0128c064c1692))
* use fixed time-ranges rather than variable on the data ([0765b82](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/0765b8287c8d57d4c4a28bab3c0f49d5744ec2db))

## [0.18.0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.17.2...v0.18.0) (2024-02-23)


### ⚠ BREAKING CHANGES

* `aggregate` function no longer takes a `node_type` argument.

### Features

* **practice:** aggregate `landUseManagement` with units `ratio` and `number` ([3336ca0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/3336ca0e010856eec9c77ddf61f037695d8bb099))


### Bug Fixes

* **cycle:** normalize all non crop products to 1 product ([0c2ee1d](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/0c2ee1d40bcaf91307973e6c48166988ea98b8ea))


* do not aggregate ImpactAssessment ([ba5341f](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/ba5341f07bb144b4c691a5aa4298f7de61bf1549))

### [0.17.2](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.17.1...v0.17.2) (2024-02-20)


### Bug Fixes

* **countries:** set min/max based on non-weighted values ([2b04461](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/2b0446169b6e5082371834b4ac8f722bfe60d39d))
* handle float precision error ([b6adf99](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/b6adf99f499e98d773e08c5cc1ceb5f7fbd77303))
* **impact assessment:** fix product containing extra information ([cf94c4e](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/cf94c4e5bcb7bb767051eb81cce1cd2585f58352))
* **impact-assessment:** add list of `inputs` per `emissionsResouceUse` ([94b8a00](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/94b8a00a9eb04e78fb2f312a758ec0f39f4a7ef6))
* limit max year to current year ([f6501aa](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/f6501aac123d92046150400787d4b9f670e25447))
* **practice:** only aggregate for certain `termType` ([8c65eb7](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/8c65eb7005e43da401060ad5377f32b992268134))
* **practice:** round values to 8 decimals ([cd3e97d](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/cd3e97d33b7a8ec67bde9eaa849c0a6ca9437b13))

### [0.17.1](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.17.0...v0.17.1) (2024-02-06)


### Bug Fixes

* **site:** ignore measurements with time-split data points ([fd50720](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/fd50720dcdcd781cf7c3196d317345247b738f9c))

## [0.17.0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.16.0...v0.17.0) (2024-01-23)


### ⚠ BREAKING CHANGES

* Min schema version required is `25.0.0`.

### Features

* **cycle:** aggregate practices ([f17e83c](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/f17e83cc52d0f81a623b9824526ca46b2a13b504))
* **emission:** implement `inputTermIdsAllowed` and `inputTermTypesAllowed` ([cb53d53](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/cb53d53994a2019c17bb2f820152ac2239aac7c2))
* **quality score:** select `completeness` fields from schema ([e131529](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/e131529606851e9a63fc6a2084b47c2b8b50d1ab))
* update to schema 25 ([84c70ad](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/84c70ad76f33d663274a61cf4e262f39caf3e606))


### Bug Fixes

* **cycle practices:** remove duplicated organic/irrigated practices ([2aa17a0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/2aa17a0d93f174e7c3ffa38f056b6cacb316570a))
* limit max aggregation year to current year ([42f2067](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/42f20671024f8214df3949f351fdef534561fbf3))

## [0.16.0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.15.6...v0.16.0) (2023-11-21)


### ⚠ BREAKING CHANGES

* **quality score:** Min schema version required is `24.0.0`.

### Bug Fixes

* **aggregation:** handle empty results ([4317c85](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/4317c85b1eb8b8659e1ff66e881497fde369c41b))
* **quality score:** filter emissions by product and siteType ([51b2b51](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/51b2b51f2f30ddc44d11a8eec736d2f9b03b1893))

### [0.15.6](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.15.5...v0.15.6) (2023-11-06)


### Features

* **cycle:** add env variable enable filter min score ([5ec152d](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/5ec152d65e1c12dacd77f31461005a3ef1df65bf))


### Bug Fixes

* **impact assessment:** skip if no linked Cycle ([65aa7e8](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/65aa7e85b3977f62920452710add4b4b18ab5d13))

### [0.15.5](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.15.4...v0.15.5) (2023-09-25)


### Features

* **cycle:** set `methodTier` on `Emission` when all aggregated have the same ([59ca86e](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/59ca86e440ea28dd9a93da12a56969cdfe9bda3f)), closes [#66](https://gitlab.com/hestia-earth/hestia-aggregation-engine/issues/66)


### Bug Fixes

* **quality score:** fix get world production on maize ([2692793](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/2692793ce45b01cabaa31a9f45ccd04727bea622))
* **quality score:** handle production share of sub-regions ([55a004f](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/55a004f1cda457e9a08335142534dbc8a2ba8790)), closes [#67](https://gitlab.com/hestia-earth/hestia-aggregation-engine/issues/67)

### [0.15.4](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.15.3...v0.15.4) (2023-08-14)


### Bug Fixes

* **cycle:** sum all values with product same `term` ([37bc968](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/37bc968f019cb0f9b0f37ad5731b96d122caf603))
* remove restriction on min quality score ([79fd048](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/79fd048af5ac41b2fffb1e126437d33e44f3a782))

### [0.15.3](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.15.2...v0.15.3) (2023-08-01)


### Features

* **cycle:** skip cycles where quality score is too low ([2ba39ae](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/2ba39aed822ad0a25d4ed527bb688462fa96dd69))
* **quality score:** include score on global represented production ([84b6534](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/84b65342e252471ac578e9017d3809103679d758))
* **quality score:** set `aggregatedQualityScoreMax` ([e9966c5](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/e9966c5e88fe25ffe88a8487ba0fbc1a2e8fbf74))


### Bug Fixes

* **quality score:** account for all emissions in system boundary ([d7122c1](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/d7122c1b8357c1913cd3ebaf8052c2375f0a464b))
* **quality score:** fix checking of all emissions included ([406b097](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/406b097c51995bbaf5f6a8a0249ed37ec9cc83d5))
* **quality score:** return delta with FAO is above threshold on non-crop products ([55a2ab8](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/55a2ab89056dc473f30a544013792fedce8edd26))

### [0.15.2](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.15.1...v0.15.2) (2023-06-19)


### Bug Fixes

* **cycle:** aggregate `product.value` ([aeddd9c](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/aeddd9c0f95b87bf72cbd41e1779a4865fee7efd))
* **impact assessment:** aggregate product `value` and `economicValueShare` ([45bd330](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/45bd33099f62d0b41120e20f96d46aabcad1afbd))

### [0.15.1](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.15.0...v0.15.1) (2023-06-15)


### Features

* **aggregation combine:** return raw pivoted dataframe ([e717f7e](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/e717f7e141aac83e976dc3da6c8f4c9c251440b0))


### Bug Fixes

* **cycle:** skip aggregate emissions not in Hestia system boundary ([5604ec0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/5604ec09ba5a5d60eba7693caac6201c791bc7b5))
* fix parsing of value ([3b34f40](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/3b34f4037a486f14525f9b6193337b3f3aa28d32)), closes [#56](https://gitlab.com/hestia-earth/hestia-aggregation-engine/issues/56)
* **queries:** handle old and new ImpactAssessment product ([92ccb75](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/92ccb7523a75ae8b2654ab6458432d690da292f3))

## [0.15.0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.14.0...v0.15.0) (2023-06-01)


### ⚠ BREAKING CHANGES

* **requirements:** min schema version is `21.1.0`

### Features

* **requirements:** update to schema `21.1.0` ([5e8072c](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/5e8072caab17eafcda0e5590fa9c64fbe06863cd))

## [0.14.0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.13.2...v0.14.0) (2023-04-05)


### ⚠ BREAKING CHANGES

* **requirements:** schema version `18` required

### Features

* **cycle:** aggregate `numberOfCycles` ([0f9def9](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/0f9def96a6227e81436f196cb4c65306d3ec5d7a))
* **requirements:** update to schema `18` ([1bd490b](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/1bd490bb5087e6c85854418d46da448ba889b2e3))


### Bug Fixes

* **quality score:** use `numberOfCycles` to calculate max observations ([03a1a91](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/03a1a91c6f2c132b06ecbce28091a2c9abcb437e))

### [0.13.2](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.13.1...v0.13.2) (2023-02-14)


### Features

* **countries:** default organic to global values and irrigated to all land type ([291a119](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/291a1192f80ee350d1fefd87fd698afff15a171a))

### [0.13.1](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.13.0...v0.13.1) (2023-01-16)


### Features

* **countries:** get irrigated weight from `agriculture` if `cropland` has no value ([b806b2e](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/b806b2eeecafebc1179c19b86320ccbe89f4de91))
* **cycle:** save list of aggregated `Cycle` and `Source` ([e488e5d](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/e488e5d1ff9ccaaf831b325b2bfcdc9fcf33086b))
* **impact assessment:** save list of aggregated `ImpactAssessment` and `Source` ([d341a91](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/d341a914e91e009ba7bab3b965e2fae8aae06712))
* **site:** save list of aggregated `Site` and `Source` ([d7f0ee1](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/d7f0ee15a92a91a90c8aa7e413da9bfbd9d63bb4))


### Bug Fixes

* **countries:** make sure weight is below `1` (100%) ([214a746](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/214a7461dc1e674db63bae807898e5664a955216))
* **cycle:** handle relative unit with product value `0` ([002fc64](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/002fc64a4c46038f37376ffe875ec827f94832f4))
* **source:** remove duplicated `aggregatedSources` ([ff925e9](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/ff925e96a63f6a6bff4a49db4bd141f23a77486b))

## [0.13.0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.12.0...v0.13.0) (2023-01-09)


### ⚠ BREAKING CHANGES

* **source:** Use new bibliography title from Hestia source.

### Features

* **requirements:** update schema to `14.0.0` ([7863e1f](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/7863e1f41ec288de0fe610e87b4acf584723e959))
* **requirements:** use schema version `13.0.0` ([c4028d7](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/c4028d7f4e6f84e00f33e69d76e2c31c9951dfae))
* **source:** use new bibliography title ([38c38c7](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/38c38c77008305b44f2b4a55c7f41266da961a82))

## [0.12.0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.11.0...v0.12.0) (2022-10-18)


### ⚠ BREAKING CHANGES

* **requirements:** schema min version is `12.0.0`

### Features

* **requirements:** use schema version `12.0.0` ([62d2792](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/62d2792c4ed3a4d97d6e8cbe7f5fb52981e0b0d7))


### Bug Fixes

* **impact assessment:** handle no cycle present ([2a55663](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/2a55663291146430c810ec4cce058bbfd630d5f3))

## [0.11.0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.10.1...v0.11.0) (2022-10-04)


### ⚠ BREAKING CHANGES

* **impact assessment:** min schema version is `11.2.0`

### Features

* **impact assessment:** weight values by `product.value` for crops ([fd34ecd](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/fd34ecd23d712747174068d201d02adb45ac5a18))

### [0.10.1](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.10.0...v0.10.1) (2022-09-20)


### Bug Fixes

* **cycle:** only set as irrigated if above `0%` irrigation ([33f50f5](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/33f50f5adfd30381eb4baab325059c66ab99eae3))

## [0.10.0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.9.3...v0.10.0) (2022-09-06)


### ⚠ BREAKING CHANGES

* **cycle:** update schema to `10.0.0`

### Features

* **cycle:** calculate `aggregatedQualityScore` ([cc03586](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/cc035866b451828b298e8857cb177a3c8d257c9e))
* **cycle:** increase threshold quality score for FAO yield to 20% ([78fee2f](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/78fee2fe28145728d4819be6f12e1a5b898e1bea))
* **impact assessment:** copy `aggregatedQualityScore` from Cycle ([31c458d](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/31c458d322c19dc13dbba10b33af51340448f3aa))


### Bug Fixes

* **cycle:** handle no observations in quality score ([5d2d66e](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/5d2d66e763da4b3c5536905a48710f820cd32c06))

### [0.9.3](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.9.2...v0.9.3) (2022-08-23)


### Bug Fixes

* **queries:** handle no latest date for product ([4d2fbad](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/4d2fbadbd68ee3cb9512a8162aa37f2e8932d4db))

### [0.9.2](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.9.1...v0.9.2) (2022-08-15)


### Bug Fixes

* **cycle:** skip aggregation if `Cycle` as no `Site` ([4200cde](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/4200cdecbc59f0bdba5d8e1271ab9556cd326880))

### [0.9.1](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.9.0...v0.9.1) (2022-08-10)


### Features

* **impact assessment:** aggregated `endpoints` ([33da0f2](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/33da0f28465ea65b6368ad3003cdfee7ab6f8f79))
* **impact assessment:** group by `methodModel` for `impacts` and `endpoints` ([cd02a9f](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/cd02a9f67e89283f45655dfed3168f807b1443d5))
* **impact assessment:** set `site` if exists ([c3ab442](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/c3ab4423064f888790f10c39c3f82769c405f4c1))


### Bug Fixes

* **world:** skip aggregation if no `weights` found ([a32bb9c](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/a32bb9c6908c35541175cc3d696eab16101172fa))

## [0.9.0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.8.9...v0.9.0) (2022-06-21)


### ⚠ BREAKING CHANGES

* **requirements:** support schema version `8.0.0` minimum

### Features

* **cycle:** handle `functionalUnit` = `relative` ([ae9c89a](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/ae9c89a9ef41d200bbfc185311d746d98efb721d))


### Bug Fixes

* **combine:** handle header without dots ([bd55bad](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/bd55bad15dc053ee7830c15a96998e71250e2623))
* **cycle:** find correct primary product ([be5a547](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/be5a5478df79393430274b9975dc16cec90dc10a))


* **requirements:** require schema version `8.0.0` ([798953c](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/798953c828ccb6cc0d373358b2fd178a65d3a545))

### [0.8.9](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.8.8...v0.8.9) (2022-06-06)


### Bug Fixes

* use `skipAggregation` for `primary` `Product` only ([0e0221f](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/0e0221f18e0d0c1003358cd17fb588fc3479b893))

### [0.8.8](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.8.7...v0.8.8) (2022-06-06)


### Bug Fixes

* **world:** fix error min/max on weigted values ([cade765](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/cade765368c6ba084a012acdf7892904423461a1))

### [0.8.7](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.8.6...v0.8.7) (2022-05-24)

### [0.8.6](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.8.5...v0.8.6) (2022-05-17)


### Bug Fixes

* **countries:** fix min/max values not set from observations ([8bb7276](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/8bb7276ecfe8948ab56ba54bd0b587ae619461e6))
* **world:** fix min/max values not set from observations ([3c8b99c](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/3c8b99c063c496d6cac16ca01b35c7f20adffec4))

### [0.8.5](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.8.4...v0.8.5) (2022-04-23)


### Bug Fixes

* **cycles:** ignore cycle where primary product has no `value` field ([63d0adc](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/63d0adc509abe99d840a4e63b52258e0a5c9f124))

### [0.8.4](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.8.3...v0.8.4) (2022-04-09)


### Features

* **combine:** add `units` for term names ([1b0b0e6](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/1b0b0e6d4a9eeed694b2588ba527bc81593035e0))


### Bug Fixes

* **combine:** handle download term not found ([fba5323](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/fba5323f29e002f8f0abdbbec25c5f74b1f20306))

### [0.8.3](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.8.2...v0.8.3) (2022-04-02)


### Features

* **utils:** handle no files to combine return `None` ([2af6da6](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/2af6da60be3c081d392c8d545feba2a4ad168ad7))

### [0.8.2](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.8.1...v0.8.2) (2022-04-02)


### Features

* **utils:** add script to combine aggregation uploads per type/product ([4bc3fe9](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/4bc3fe92b68bc74c0753dbae4e86868a85b789e2))


### Bug Fixes

* **models:** sum up `observations` to get total ([4bbc450](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/4bbc450c520503a05f833a2ea9d33bb5a815520c))

### [0.8.1](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.8.0...v0.8.1) (2022-03-14)


### Bug Fixes

* **queries:** only use countries to aggregate for the World ([039b638](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/039b638cc7c156f1746420fe574363d946f07102))

## [0.8.0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.7.4...v0.8.0) (2022-02-28)


### ⚠ BREAKING CHANGES

* **aggregation:** `get_time_ranges` uses `country` instead of `country_name`

### Features

* **aggregation:** handle aggregating by continent ([fca3bde](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/fca3bdeb339a141c9eaadce6d53036397ad43bdf))
* **log:** disable logging to console by default ([833b2b9](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/833b2b9020744d571ff3d5240d047bf7010f1621))

### [0.7.4](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.7.3...v0.7.4) (2022-02-21)


### Features

* **cycle:** average `economicValueShare` on primary prod ([0436d92](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/0436d920b219c01f5749cc0f40fec79a41527cec))


### Bug Fixes

* **cycle:** skip cycles primary product without `economicValueShare` ([fb61dc6](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/fb61dc62ca1335d6c5ff6ca637f8512567565aca))
* **models:** account for all values in `observations` ([412a464](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/412a464a2dee654825314f28b7311f65041a3be7))

### [0.7.3](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.7.2...v0.7.3) (2022-01-23)


### Bug Fixes

* **impact assessment:** handle `null` values ([2bd1cc0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/2bd1cc0558ecfcb4db17852cc2ddf2d0f818a685))

### [0.7.2](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.7.1...v0.7.2) (2022-01-12)


### Features

* **impact assessment:** link to aggregated Cycle ([c98f522](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/c98f52294f343f6b079bdb33de3b02eb35023650))

### [0.7.1](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.7.0...v0.7.1) (2022-01-12)


### Features

* **queries:** return time ranges bound to actual available data ([b7f5d3c](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/b7f5d3c2ace3cda7de28e4107737dd1716693ecd))


### Bug Fixes

* **impact assessment:** handle no impacts after grouping ([3166e17](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/3166e17a71cc7c7818f31e89cb40d775a15b7211))
* **impact assessment:** sum identical emissions before aggregating ([0420001](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/0420001af941fc7d7d197c67970e2bdd457f5aff))

## [0.7.0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.6.2...v0.7.0) (2021-11-06)


### ⚠ BREAKING CHANGES

* **world:** current version requires glossary above `0.0.11`

### Bug Fixes

* **world:** use new crop grouping column name ([e178974](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/e178974feeb68f3f67fa0cf7ae0efac48ab9f021))

### [0.6.2](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.6.1...v0.6.2) (2021-11-04)


### Bug Fixes

* **cycle:** handle no cycles after aggregating by term ([56192f0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/56192f05cda11e2ed0a2096e3dfd298d4b8af2e0))
* **log:** handle total weight of 	0 ([3ffdeb7](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/3ffdeb747bbf533a5791b327fef2732c007e4bca))

### [0.6.1](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.6.0...v0.6.1) (2021-11-01)


### Features

* **cycle:** skip aggregate for `relative` functional unit ([3b42844](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/3b428441bfeb0fff7902100d198dcdf2a175a30b))
* **world:** account for completeness add missing `0` values ([88f2cd7](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/88f2cd715371315d375c11d5084d706a1eecad26))


### Bug Fixes

* **aggregation:** skip run if no nodes found ([d31aa18](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/d31aa18fefcdd70027cb1ec94dc77009a4d029dd))
* **countries:** handle no organic/irrigated lookup factor default to `0` ([0f57795](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/0f577952dab34eec259fcee74d427d0c38c42d07))
* **cycle:** skip aggregate if no grouping by product ([e80af89](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/e80af89d92e47bad178ff3e19be0f98790fd8200))
* fix group completeness on selected node only ([a451816](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/a45181626c32313ecc2534487436760cc70327bd))

## [0.6.0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.5.4...v0.6.0) (2021-10-25)


### ⚠ BREAKING CHANGES

* aggregate with a `product` and `start_year` + `end_year`

### Features

* **impact assessment:** aggregate `impacts` ([145b6ce](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/145b6ce0bb95233169291320b59b8cd8d0ddb8f4))
* **terms:** acount for `dataCompleteness` for missing data ([7e51c9f](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/7e51c9f8e4e0ea685c98b53a0740503b170a701b))
* **utils:** add function to get all countries ([d4bed77](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/d4bed77427c6085f5a8781d48269d83a21441a76))


### Bug Fixes

* **log:** set log level for file logs ([62881f6](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/62881f68723fe2c78fd5e7800abb1684e1f86366))


* run aggregation per type, country, product and time range ([c5ba34d](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/c5ba34ddb92de9796ecda40f9da399d0dae2e83c))

### [0.5.4](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.5.3...v0.5.4) (2021-10-21)


### Bug Fixes

* **cycle:** handle no `inputs` ([90abe28](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/90abe28fb95f40387a8b4344e9d2a9492fdd0c57))

### [0.5.3](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.5.2...v0.5.3) (2021-10-15)


### Bug Fixes

* **cycle utils:** handle organic and irrigated practices together ([ad4ed8b](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/ad4ed8bb6246f2e9635b3b84d46ef56f82fff710))

### [0.5.2](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.5.1...v0.5.2) (2021-10-13)


### Features

* **terms:** skip `deleted` nodes ([ccaa2af](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/ccaa2af95b2ed3483c00ebdd16a5d572698d9748))


### Bug Fixes

* **models term:** skip aggregate without `value` ([ce9043a](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/ce9043a0f6831b9660f98f8c3fac02ad389302c2))

### [0.5.1](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.5.0...v0.5.1) (2021-09-06)


### Bug Fixes

* **cycle:** handle non-recalculated (aggregated) site ([737b2a8](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/737b2a8bbb62fef6d15225f38f9267f0d9c354dc))
* **utils:** fix group by product skip non-aggregated ([179180c](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/179180c8e8895ade22f7d7bb4babe8ba74e5c61a))

## [0.5.0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.4.0...v0.5.0) (2021-08-14)


### ⚠ BREAKING CHANGES

* use schema version `6.5.0` minimum

### Features

* require schema `6.5.0` ([fb636a5](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/fb636a5fd967ec445647ce238c3a95bdc8cda484))


### Bug Fixes

* **aggregation:** skip run if no time ranges ([5622039](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/5622039624c2827e713c9d2b69d12e092d651ecb)), closes [#16](https://gitlab.com/hestia-earth/hestia-aggregation-engine/issues/16)
* **cycle utils:** handle no inputs/products/emissions ([edce6cd](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/edce6cda812245bbb765345df11a28c509fe3c24))
* **cycle utils:** skip practices for country aggregations ([46ac395](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/46ac39534aa699bb7fdfd7505c42979810d931c2))
* **cycle:** handle `site` with no aggregated `measurements` ([73c2b36](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/73c2b36fc04c4f4f3ef4f2698f64a5f9c99b8086))
* **cycle:** handle no `primary` product ([7e24e8a](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/7e24e8aa9a0b7e8542559de09af746d6be3e8d29))
* **cycle:** skip adding `defaultSource` from uploaded cycles ([107bb44](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/107bb449ca7e24df48c0fb40ab3af17595b5908e))
* **impact assessment:** handle no aggregated data ([b7a7cdb](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/b7a7cdb429db482eca12fa34bb52c28832b35ba9))
* **model terms:** fix no aggregates results ([21c7108](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/21c7108a8ab12fb709083c43c4f31adcd78fadab))
* **models:** handle data with no `value` ([18a2837](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/18a2837be95f41adea6e994dc089c90c7843bc9e))
* only set `statsDefinition` if `value` is set ([ebbe4d0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/ebbe4d0d6e14d9f5595dc6b84cc2a13070c4b90b))

## [0.4.0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.3.0...v0.4.0) (2021-07-23)


### ⚠ BREAKING CHANGES

* use schema min `6.0.0`

* handle `practice.value` as an array ([bca48a8](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/bca48a88f32023d9b848f04425f92992126b9967))

## [0.3.0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.2.1...v0.3.0) (2021-07-22)


### ⚠ BREAKING CHANGES

* use schema min `5.3.0`

### Features

* restrict aggregated data to the same schema major version ([7764fbd](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/7764fbdd54e9a4150107692fa080de2d88bd399b))
* use schema `5.3.0` ([5dc492a](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/5dc492a7ef210c49c988528fd2e12a6ddea2ef6f))

### [0.2.1](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.2.0...v0.2.1) (2021-07-19)


### Features

* aggregate `min` and `max` over 20 observations ([6684358](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/6684358a195ab573f03411241fec71546660dca7))
* aggregate `sd` over `2` observations ([0a259ab](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/0a259ab5c570821551a9ef608e3b0a1ef2aa8fab))
* **cycle:** aggregate `dataCompleteness` ([f01b747](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/f01b7475eb1021a4ce588ca59be67291e49557ed))
* **cycle:** aggregate `emissions` ([0d2364a](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/0d2364a69cc9b2e08a2399a5c7a44a6355a70e54))
* **requirements:** use utils `0.8.1` minimum ([de2726f](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/de2726f81dc3a272daebf0a83af31ba772a7e1bd))
* set `aggregated` and `aggregatedVersion` on top Nodes ([50f6f88](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/50f6f88d5d4d379267a4a924b9108cd75a838bb9))

## [0.2.0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.1.2...v0.2.0) (2021-07-16)


### Features

* **cycle:** aggregate by country and World ([17d737d](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/17d737d6e4f74c0a1cf32ed746f40697a71bd5ba))
* **site:** aggregate by country and global ([a086b54](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/a086b541ad8b66f6533a678712bd61d26b53b68d))


### Bug Fixes

* **impact assessment:** fix generation `id` follow name ([821a36a](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/821a36ade28ef5755642cd384c8dae2458c3069a))
* **terms:** handle any terms ([a4e73c5](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/a4e73c5b1b65720fffb11436b872ae9562c0138e))

### [0.1.2](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.1.1...v0.1.2) (2021-07-08)


### Features

* **impact assessment:** change time range to start from decade ([950b5a5](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/950b5a5ce515fe8347883a0966187ae39de5a784))
* **impact assessment:** format name following Cycle standard ([5779ff0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/5779ff0f8a038b569e01df5dc3e06fe30b0fd098))


### Bug Fixes

* **impact assessment:** normalize country name in generated `id` ([31743cb](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/31743cbdcce3189041d114e3cd85f7a522f453c4))

### [0.1.1](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.1.0...v0.1.1) (2021-06-04)


### Bug Fixes

* **impact assessments:** ignore existing world data when computing world data ([b012757](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/b012757617be8472998ae919c5f555dbdb66bf7a))

## [0.1.0](https://gitlab.com/hestia-earth/hestia-aggregation-engine/compare/v0.0.1...v0.1.0) (2021-05-05)


### Features

* return `termType` key for terms ([afc8d38](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/afc8d386d6c142456b2e77fa65cbf2501604b38a))
* **aggregate:** handle start/end year for finding impacts ([f014e88](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/f014e885932d112c22e06b3c966b789f5e3b4c77))
* **aggregate country:** handle country not found ([f96cf01](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/f96cf014595068abc5c2f21ca6295858967328ff))
* **aggregation:** aggregate over time period ([61e372e](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/61e372e939ce9c45946d6ca3ea8e76c8295d65de))
* **impact assessment:** export function to return time ranges ([3db966c](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/3db966c67b164d49d52a8594321605289aa354b2))
* **models:** set `observations` and `statsDefinition` ([22d8f6d](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/22d8f6d31c10fde35c47a91981374dd5a76776ba))
* **world:** update crop grouping column name ([93c9423](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/93c9423b775ad312e995a95b1fb9fa94da877a7a))


### Bug Fixes

* **aggregation:** handle no time ranges for a country ([a21a9a8](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/a21a9a8ab7a4c13c4c653fbe18f66375cd4fd1e7))
* **countries:** handle lookup without data ([fb1bda5](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/fb1bda55c87d1271ab3571e04fb7645460bf3db0))
* **wolrd:** weight value from country production over global production ([79c7d4b](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/79c7d4ba8b40402ead12fe93613a065a367c2e99))

### 0.0.1 (2021-04-06)


### Features

* group by organic/irrigated products ([a4ff860](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/a4ff860f88c4cf02835b6abeb8b2b195c65c6ae6))
* **aggregation:** aggregate value by country for `emission` and `resourceUse` ([eeea64f](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/eeea64ff90d8faff041559d5dc8ada0da46c5744))
* **aggregation:** handle country and global aggregations ([e49e00d](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/e49e00dbf4929b52c8298e6760cc1b75b5755f13))
* **model world:** handle non-weighted aggregation ([3c05171](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/3c05171ba2bbeb9468f1e38c682dd733dfc79c2e))
* **terms model:** handle weighted organic/irrigated values ([588b77f](https://gitlab.com/hestia-earth/hestia-aggregation-engine/commit/588b77fbec078eda738dbc6ea1813f41fed52f8b))
