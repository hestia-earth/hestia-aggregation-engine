# usage:
# 1. Install python-dotenv and pandas
# 2. Set your env variables in a `.env` file
# 3. Run `python combine_aggregations.py <Cycle or ImpactAssessment> <product id> <dest folder>`
from dotenv import load_dotenv
load_dotenv()


import sys
import os
from hestia_earth.aggregation.utils.combine import run

BUCKET = os.getenv('AWS_BUCKET_UPLOADS')


def main(args: list):
    node_type, product_id, dest_folder = args
    df = run(BUCKET, node_type, product_id)
    dest_filename = f"{'-'.join([node_type.lower(), product_id])}.csv"
    df.to_csv(os.path.join(dest_folder, dest_filename))


if __name__ == "__main__":
    main(sys.argv[1:])
