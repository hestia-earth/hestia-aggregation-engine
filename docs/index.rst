Welcome to HESTIA Aggregation Engine's documentation!
========================================

This package contains the Aggregation engine developed by HESTIA.

Installation
------------

Install from `PyPI <https://pypi.python.org/pypi>`_ using `pip <http://www.pip-installer.org/en/latest/>`_, a
package manager for Python.

.. code-block:: bash

    pip install hestia_earth.aggregation


Requirements
============

- `hestia_earth.schema >= 3.2.0 <https://pypi.org/project/hestia-earth.schema/>`_
- `hestia_earth.utils >= 0.4.9 <https://pypi.org/project/hestia-earth.schema/>`_
- `requests >= 2.24.0 <https://pypi.org/project/requests/>`_

Contents
--------

.. autosummary::
   :toctree: _autosummary
   :caption: API Reference
   :template: custom-module-template.rst
   :recursive:

   hestia_earth.aggregation


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
