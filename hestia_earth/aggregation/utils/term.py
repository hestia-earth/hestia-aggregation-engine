import re
from unidecode import unidecode
from hestia_earth.schema import SchemaType, TermTermType
from hestia_earth.utils.model import linked_node, find_term_match
from hestia_earth.utils.api import find_node, find_node_exact, download_hestia
from hestia_earth.utils.lookup import download_lookup, get_table_value, column_name
from hestia_earth.utils.tools import safe_parse_float

SEARCH_LIMIT = 10000
DEFAULT_COUNTRY_ID = 'region-world'
DEFAULT_COUNTRY_NAME = 'World'
DEFAULT_COUNTRY = {'@id': 'region-world', 'name': DEFAULT_COUNTRY_NAME}
MODEL = 'aggregatedModels'
METHOD_MODEL = {'@type': SchemaType.TERM.value, '@id': MODEL}
DRY_MATTER_TERM_ID = 'dryMatter'


def _fetch_all(term_type: TermTermType): return find_node(SchemaType.TERM, {'termType': term_type.value}, SEARCH_LIMIT)


def _fetch_single(term_name: str): return find_node_exact(SchemaType.TERM, {'name': term_name})


def _fetch_default_country(): return _fetch_single(DEFAULT_COUNTRY_NAME)


def _fetch_countries():
    return find_node(SchemaType.TERM, {
        'termType': TermTermType.REGION.value,
        'gadmLevel': 0
    }, SEARCH_LIMIT)


def _format_country_name(name: str):
    return re.sub(r'[\(\)\,\.\'\"]', '', unidecode(name).lower().replace(' ', '-')) if name else None


def _format_organic(organic: bool): return 'organic' if organic else 'conventional'


def _format_irrigated(irrigated: bool): return 'irrigated' if irrigated else 'non-irrigated'


def _is_global(country: dict): return country.get('@id', '').startswith('region-')


def should_aggregate(term: dict):
    lookup = download_lookup(f"{term.get('termType')}.csv", True)
    value = get_table_value(lookup, 'termid', term.get('@id'), column_name('skipAggregation'))
    return True if value is None or value == '' else not value


def update_country(country_name: str):
    return linked_node({
        **(_fetch_single(country_name) if isinstance(country_name, str) else country_name),
        '@type': SchemaType.TERM.value
    })


def _term_lookup_dm(term: dict):
    lookup = download_lookup(f"{term.get('termType')}-lookup.csv")
    return safe_parse_float(get_table_value(lookup, 'termid', term.get('@id'), column_name(DRY_MATTER_TERM_ID)))


def _term_dm(term: dict):
    data = download_hestia(term.get('@id'))
    return safe_parse_float(find_term_match(data.get('defaultProperties', []), DRY_MATTER_TERM_ID).get('value'))


def term_dryMatter(term: dict): return _term_lookup_dm(term) or _term_dm(term)
