#!/bin/sh

FROM_BRANCH="develop"
TO_BRANCH="master"

git pull --rebase
git checkout $TO_BRANCH
git merge $FROM_BRANCH
npm run release
git checkout $FROM_BRANCH
git merge $TO_BRANCH
git push
