#!/bin/sh
docker build --progress=plain \
  -t hestia-aggregation-engine:latest \
  .
docker run --rm \
  --name hestia-aggregation-engine \
  -v ${PWD}:/app \
  hestia-aggregation-engine:latest python run.py "$@"

npm run convert:csv
