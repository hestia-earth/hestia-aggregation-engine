#!/bin/sh
pytest -n 5 \
  --cov-config=.coveragerc \
  --cov-report term \
  --cov-report html \
  --cov-report xml \
  --cov=./ \
  --cov-fail-under=90
