# usage:
# 1. Install python-dotenv
# 2. Set your env variables in a `.env` file
# 3. Run `python run.py Cycle <product name> <country or "World" for all> 2000 2009`
from dotenv import load_dotenv
load_dotenv()


import argparse
import json
import sys
import logging

from hestia_earth.schema import SchemaType
from hestia_earth.utils.api import download_hestia, find_node
from hestia_earth.utils.tools import non_empty_list
from hestia_earth.aggregation.log import logger
from hestia_earth.aggregation import aggregate
from hestia_earth.aggregation.utils.queries import get_time_ranges, count_nodes
from hestia_earth.aggregation.utils.cycle import _timestamp


parser = argparse.ArgumentParser()
parser.add_argument('--output-folder', type=str,
                    default='samples',
                    help='The folder where to store the results. Defaults to "samples"')
parser.add_argument('--filepath', type=str,
                    help='Full filepath of the aggregations, like "riceGrainInHuskFlooded-GADM-THA-2010-2024"')
parser.add_argument('--id', type=str,
                    help='The ID of an existing aggregated node, like "riceGrainInHuskFlooded-thailand-2010-2024"')
parser.add_argument('--product-name', type=str,
                    help='Product "name" from HESTIA glossary')
parser.add_argument('--product-id', type=str,
                    help='Product "@id" from HESTIA glossary')
parser.add_argument('--country-name', type=str,
                    help='Country "name" from HESTIA glossary')
parser.add_argument('--country-id', type=str,
                    help='Country "@id" from HESTIA glossary')
parser.add_argument('--start-year', type=int,
                    help='Starting year for timerange')
parser.add_argument('--end-year', type=int,
                    help='Ending year for timerange')
parser.add_argument('--count-only', action='store_true',
                    help='Return only the count of nodes')
args = parser.parse_args()

source = {
    '@type': 'Source',
    '@id': 'pu2wmwp8yfv7',
    'name': 'Hestia Team (2023)'
}


def _find_by_name_or_id(id: str = None, name: str = None):
    term_id = id or (find_node(SchemaType.TERM, {'name': name}, limit=1) or [{}])[0].get('@id')
    return download_hestia(term_id)


def _get_years(country: dict, product: dict):
    [start_year, end_year] = [
        args.start_year,
        args.end_year
    ] if all([args.start_year, args.end_year]) else get_time_ranges(country, product.get('name'))[0]
    return (start_year, end_year)


def main():
    # print everything to console as disabled by default
    logger.addHandler(logging.StreamHandler(sys.stdout))
    logger.setLevel(logging.getLevelName('DEBUG'))

    end_year = None

    if args.filepath:
        parts: list = args.filepath.split('-')
        product_id = parts.pop(0)
        product = _find_by_name_or_id(id=product_id)
        end_year = parts.pop()
        start_year = parts.pop()
        country_id = '-'.join(parts)
        country = _find_by_name_or_id(id=country_id, name=country_id)
    elif args.id:
        parts: list = args.id.split('-')
        product_id = parts.pop(0)
        product = _find_by_name_or_id(id=product_id)
        # handle timestamp
        if len(str(parts[-1])) == 8:
            parts.pop()
        end_year = parts.pop()
        start_year = parts.pop()
        country_name = '-'.join(parts)
        country = _find_by_name_or_id(name=country_name)
    else:
        country = _find_by_name_or_id(args.country_id, args.country_name)
        product = _find_by_name_or_id(args.product_id, args.product_name)

    if not country:
        raise Exception('Please either provide --country-name or --country-id arguments.')

    if not product:
        raise Exception('Please either provide --product-name or --product-id arguments.')

    if not end_year:
        start_year, end_year = _get_years(country, product)

    if args.count_only:
        count = count_nodes(product=product, start_year=int(start_year), end_year=int(end_year), country=country)
        print("Found", count, "nodes to aggregate")
        return

    aggregates = aggregate(
        country=country, product=product, start_year=int(start_year), end_year=int(end_year), source=source
    )

    filename = '-'.join(non_empty_list([
        product.get('@id'), country.get('@id'), str(start_year), str(end_year), _timestamp()
    ]))
    path = f"{args.output_folder}/{filename}.json"
    with open(path, 'w') as f:
        f.write(json.dumps(aggregates, indent=2))


if __name__ == "__main__":
    main()
