const { readdirSync, lstatSync, readFileSync, writeFileSync } = require("fs");
const { resolve, join } = require("path");

const ROOT = resolve(join(__dirname, ".."));
const FIXTURES_FOLDER = join(ROOT, "tests", "fixtures");

const isDir = (folder) => lstatSync(folder).isDirectory();

const listFiles = (value) =>
  isDir(value)
    ? readdirSync(value)
        .map((v) => join(value, v))
        .flatMap(listFiles)
    : value;

const fixtures = listFiles(FIXTURES_FOLDER).filter((v) =>
  v.endsWith(".jsonld")
);

const ignoreKeys = [
  "added",
  "addedVersion",
  "updated",
  "updatedVersion",
  "originalId",
  "schemaVersion",
  "@context",
  "createdAt",
  "updatedAt",
  "boundary",
];

const cleanNode = (node) =>
  typeof node === "object"
    ? Object.fromEntries(
        Object.entries(node)
          .filter(([key]) => !ignoreKeys.includes(key))
          .map(([key, value]) => [
            key,
            Array.isArray(value)
              ? value.map(cleanNode)
              : value["@type"]
              ? cleanNode(value)
              : value,
          ])
      )
    : node;

const cleanFixture = (filepath) => {
  const data = JSON.parse(readFileSync(filepath, "utf-8"));
  const content = data.nodes
    ? { nodes: data.nodes.map(cleanNode) }
    : Array.isArray(data)
    ? data.map(cleanNode)
    : cleanNode(data);

  console.log("Cleaning up", filepath);
  writeFileSync(filepath, JSON.stringify(content, null, 2), "utf-8");
};

fixtures.map(cleanFixture);
