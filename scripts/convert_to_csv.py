import argparse
import json
from hestia_earth.utils.pivot.pivot_csv import nodes_to_df

parser = argparse.ArgumentParser()
parser.add_argument('--filepath', type=str,
                    help='Path of the file to convert.')
args = parser.parse_args()


def main():
    with open(args.filepath, 'r') as f:
        nodes = json.load(f)

    dest_filepath = args.filepath.replace('.json', '.csv')

    nodes_to_df(nodes).to_csv(dest_filepath)


if __name__ == "__main__":
    main()
