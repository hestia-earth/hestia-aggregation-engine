require('dotenv').config();
const axios = require('axios');
const { productTermTermType, NodeType } = require('@hestia-earth/schema');

const { listFolder, deleteObjects } = require('./utils');

const API_URL = process.env.API_URL;
const bucket = process.env.AWS_BUCKET;

const findProducts = async () => {
  const query = {
    bool: {
      must: [
        {
          match: { '@type': NodeType.Term }
        }
      ],
      should: productTermTermType.term.map(type => ({'match': {'termType.keyword': type}})),
      minimum_should_match: 1
    }
  };
  const { data: { results } } = await axios.post(`${API_URL}/search`, {
    limit: 10000,
    fields: ['@type', '@id'],
    query
  });
  return results.flatMap(res => res['@id']);
};

const findObjects = async (product) => (await Promise.all(
  [
    NodeType.Site,
    NodeType.Cycle
  ].map(type => listFolder(bucket, `recalculated/${type}/${product}-`))
)).flat();

const run = async () => {
  const ids = await findProducts();
  const objects = (await Promise.all(ids.map(findObjects))).flat();
  console.log(objects);
  await deleteObjects(bucket, objects);
};

run().then(() => process.exit(0)).catch(err => {
  console.error(err);
  process.exit(1);
});
