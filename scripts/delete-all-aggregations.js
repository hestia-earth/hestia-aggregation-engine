require('dotenv').config();
const axios = require('axios');

const API_URL = process.env.API_URL;
const JSON_LD_API_URL = process.env.JSON_LD_API_URL;
const JSON_LD_API_KEY = process.env.JSON_LD_API_KEY;

const fetchAll = async () => {
  const { data: { results } } = await axios.post(`${API_URL}/search`, {
    limit: 1000,
    fields: ['@type', '@id'],
    query: {
      bool: {
        must: [{
          match: { 'aggregated': true }
        }]
      }
    }
  });
  return results;
};

const run = async () => {
  const nodes = await fetchAll();
  return axios.delete(`${JSON_LD_API_URL}/node`, {
    data: nodes,
    headers: {
      'x-api-key': JSON_LD_API_KEY
    }
  });
};

run().then(() => process.exit(0)).catch(err => {
  console.error(err);
  process.exit(1);
});
