const { listFolder, deleteObjects } = require('./utils');

const [date, env] = process.argv.slice(2);

const deleteFiles = async (bucket) => {
  const allFiles = await listFolder(bucket);
  const files = allFiles.filter(file => file.LastModified < new Date(date));
  console.log('Found', files.length, 'aggregation files to delete in', bucket);
  return files.length && await deleteObjects(bucket, files);
};

const run = async () => {
  await deleteFiles(env === 'dev' ? 'hestia-uploads-dev' : `hestia-${env}-uploads`);
  await deleteFiles(env === 'dev' ? 'hestia-data-dev' : `hestia-${env}-data`);
};

run().then(() => process.exit(0)).catch(err => {
  console.error(err);
  process.exit(1);
});
