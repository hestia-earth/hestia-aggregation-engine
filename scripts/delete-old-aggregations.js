require('dotenv').config();
const axios = require('axios');

const [date, skipDelete] = process.argv.slice(2);

const API_URL = process.env.API_URL;
const JSON_LD_API_URL = process.env.JSON_LD_API_URL;
const JSON_LD_API_KEY = process.env.JSON_LD_API_KEY;

const maxDate = date || new Date().toJSON().substring(0, 10);
const query = {
  bool: {
    must: [
      {
        match: {
          aggregated: true
        }
      },
      {
        range: {
          createdAt: { lt: maxDate }
        }
      }
    ]
  }
};

const fetchAll = async () => {
  const { data } = await axios.post(`${API_URL}/count`, { query });
  console.log('Total results', data);
  const { data: { results } } = await axios.post(`${API_URL}/search`, {
    limit: 100,
    fields: ['@type', '@id'],
    query
  });
  return results;
};

const deleteAggregation = (nodes) => skipDelete === 'true' ? true : axios.delete(`${JSON_LD_API_URL}/node`, {
  headers: { 'x-api-key': JSON_LD_API_KEY },
  data: { nodes }
});

const run = async () => {
  const aggregations = await fetchAll();
  console.log('Deleting', aggregations.length, 'Nodes...');
  console.log(aggregations);
  return await deleteAggregation(aggregations);
};

run().then(() => process.exit(0)).catch(err => {
  console.error(err);
  process.exit(1);
});
