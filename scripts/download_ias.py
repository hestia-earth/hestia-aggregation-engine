from dotenv import load_dotenv
load_dotenv()


from hestia_earth.schema import NodeType
from hestia_earth.utils.api import search, download_hestia
from hestia_earth.utils.model import find_term_match
from hestia_earth.utils.tools import list_sum
from pandas.io.json import json_normalize


product_name = 'Wheat, grain'
country_name = 'United Kingdom'
ias = search({
    'bool': {
        'must': [
            {
                'match': {'@type': NodeType.IMPACTASSESSMENT.value}
            },
            {
                'bool': {
                    # handle old ImpactAssessment data
                    'should': [
                        {'match': {'product.name.keyword': product_name}},
                        {'match': {'product.term.name.keyword': product_name}}
                    ],
                    'minimum_should_match': 1
                }
            },
            {
                'match': {'country.name.keyword': country_name}
            }
        ],
        'must_not': [
            {
                'match': {'aggregated': True}
            }
        ]
    }
}, limit=1000)


results = []


def region(cycle: dict):
    site_id = cycle.get('site', {}).get('@id')
    if site_id:
        return (download_hestia(site_id, NodeType.SITE) or {}).get('region', {}).get('name', '')
    return ''


def cycle_yield(cycle_id: str):
    cycle = download_hestia(cycle_id, NodeType.CYCLE)
    return list_sum(find_term_match(cycle.get('products', []), 'wheatGrain').get('value'))


def impact_indicator(impact: dict, term_id: str):
    indicator = find_term_match(impact.get('impacts', []), term_id)
    return indicator.get('value')


for ia in ias:
    impact = download_hestia(ia.get('@id'), NodeType.IMPACTASSESSMENT, data_state='recalculated')
    country = impact.get('country', {}).get('name')
    cycle_id = impact.get('cycle', {}).get('@id')

    if cycle_id:
        cycle = download_hestia(cycle_id, NodeType.CYCLE)

        if cycle:
            product_value = list_sum(impact.get('product', {}).get('value', [])) or cycle_yield(cycle_id)
            results.append({
                'HESTIA ID': impact.get('@id'),
                'HESTIA Site ID': cycle.get('site', {}).get('@id'),
                'Region': region(cycle),
                'Date': impact.get('endDate'),
                'Poduct yield': product_value,
                'GWP (kg CO2eq)': impact_indicator(impact, 'gwp100'),
                'Biodiversity (PDF*year)': impact_indicator(impact, 'damageToTerrestrialEcosystemsTotalLandUseEffects'),
                'Eutrophication (kg PO43-eq)': impact_indicator(impact, 'eutrophicationPotentialExcludingFate'),
                'Acidification (kg SO2eq)': impact_indicator(impact, 'terrestrialAcidificationPotentialExcludingFate'),
                'Water Use (Leq)': impact_indicator(impact, 'scarcityWeightedWaterUse')
            })


df = json_normalize(results)
df.to_csv('sample.csv', index=False, na_rep='-')
