const { readdirSync, readFileSync } = require('fs');
const { join } = require('path');

const ROOT = join(__dirname, '..');
const SAMPLES_DIR = join(ROOT, 'samples')

const [value] = process.argv.slice(2);

const filterNodes = (filepath) => {
  const { nodes } = JSON.parse(readFileSync(filepath, 'utf-8'));
  return nodes.filter(node => JSON.stringify(node).includes(value));
};

const run = () => {
  const files = readdirSync(SAMPLES_DIR).filter(f => f.startsWith('nodes-')).map(f => join(SAMPLES_DIR, f));
  const nodes = files.flatMap(filterNodes);
  console.log(nodes.map(node => ({ '@type': node['@type'], '@id': node['@id'] })));
};

run();
