const values = [

  {
    "_id": "6283aeba5cd2e6001ab65eeb",
    "filename": "Wheat_UK_Smith_Glithero.xlsx",
    "filepath": "624eea0769b9420013214fa6/UK_Wheat_Smith/Wheat_UK_Smith_Glithero.xlsx"
  }
  ,

  {
    "_id": "628b99f3e7d6e0001b6c0ea2",
    "filename": "Wheat_UK_Berry_Fitton.xlsx",
    "filepath": "624eea0769b9420013214fa6/UK_Wheat_ADAS/Wheat_UK_Berry_Fitton.xlsx"
  }
  ,

  {
    "_id": "62ab9cd8202df4001337594b",
    "filename": "Eady et al (2012) - EXPORT v2.xlsx",
    "filepath": "5eb26d6bc3513500132349b3/Eady_et_al_2012/Eadyetal2012-EXPORTv2.xlsx"
  }
  ,

  {
    "_id": "62aba0c5202df40013375ee4",
    "filename": "Ridoutt et al (2013) v2.xlsx",
    "filepath": "5eb26d6bc3513500132349b3/Ridoutt_et_al_2013/Ridouttetal2013v2.xlsx"
  }
  ,

  {
    "_id": "62b57a93fdc5930013645368",
    "filename": "Wheat_UK_Glithero.xlsx",
    "filepath": "624eea0769b9420013214fa6/UK_Wheat_Glithero/Wheat_UK_Glithero.xlsx"
  }
  ,

  {
    "_id": "62b5c066fdc5930013646ee4",
    "filename": "Samardzic et al (2014) v13 - recalc.xlsx",
    "filepath": "5eb26d6bc3513500132349b3/Samardzik_et_al_2014_recalc/Samardzicetal2014v13-recalc.xlsx"
  }
  ,

  {
    "_id": "62c7e212b62724001aef9200",
    "filename": "Chicken_Australia_Wiedemann_2017_ V4.csv",
    "filepath": "6047bfd4d0c71900136f3873/Mondra_Cicken_Australia/Chicken_Australia_Wiedemann_2017__V4.csv"
  }
  ,

  {
    "_id": "62cebe7d5ae6f3001979c9d5",
    "filename": "Aguilera et al 2014 V4_lug22.xlsx",
    "filepath": "5fcf3fee574d760013166bb6/Aguilera_et_al_2014/Aguilera_et_al_2014_V4_lug22.xlsx"
  }
  ,

  {
    "_id": "62cec7865ae6f3001979d989",
    "filename": "LCA_asian_aquaculture_v3_lug22.xlsx",
    "filepath": "5fcf3fee574d760013166bb6/Aquaculture_Asia/LCA_asian_aquaculture_v3_lug22.xlsx"
  }
  ,

  {
    "_id": "62ceca355ae6f3001979fe3f",
    "filename": "LCA_asian_aquaculture_v3_lug22.xlsx",
    "filepath": "5fcf3fee574d760013166bb6/Archer_and_Halvorson_2010/LCA_asian_aquaculture_v3_lug22.xlsx"
  }
  ,

  {
    "_id": "62cee1e85ae6f300197a2804",
    "filename": "Vellinga et al (2013)_lug22.xlsx",
    "filepath": "5fcf3fee574d760013166bb6/Vellinga_et_al_2013/Vellinga_et_al_2013_lug22.xlsx"
  }
  ,

  {
    "_id": "62cfec7f5ae6f300197a73f4",
    "filename": "Egypt WorldFish dataset_lug22.xlsx",
    "filepath": "5fcf3fee574d760013166bb6/Egypt_aquaculture_WorldFish/Egypt_WorldFish_dataset_lug22.xlsx"
  }
  ,

  {
    "_id": "62d00dc15ae6f300197a8ee6",
    "filename": "Mungkung et al (2013)_v5Lug22.xlsx",
    "filepath": "5fcf3fee574d760013166bb6/Mungkung-2013/Mungkung_et_al_2013_v5Lug22.xlsx"
  }
  ,

  {
    "_id": "62d18b329a669e001aa5d2d5",
    "filename": "portocarerro_et_al VC.csv",
    "filepath": "622890e08454c8001f3cb06d/NitrateLeachingStudies/portocarerro_et_al_VC.csv"
  }
  ,

  {
    "_id": "62ebd113f50404001bac9281",
    "filename": "Sunflower_Serbia_Login5_v9.xlsx",
    "filepath": "626256d7ecd680001a22df2b/2022/Sunflower_Serbia_Login5_v9.xlsx"
  }
  ,

  {
    "_id": "62ebfbcbf50404001bacae2a",
    "filename": "Sunflower_Serbia_Login5_v11.xlsx",
    "filepath": "5fcf3fee574d760013166bb6/Sunflower_Serbia_L5/Sunflower_Serbia_Login5_v11.xlsx"
  }
  ,

  {
    "_id": "62f524dc799929001791282c",
    "filename": "Cardoso et al (2016).xlsx",
    "filepath": "5fcf3fee574d760013166bb6/Cardoso2016/Cardoso_et_al_2016.xlsx"
  }
  ,

  {
    "_id": "62f9f96c31195e001c6c2cba",
    "filename": "Ultimate_File_V5.xlsx",
    "filepath": "5ef227ebcb11440013549042/Master/Ultimate_File_V5.xlsx"
  }
  ,

  {
    "_id": "62f9fe7931195e001c6c3c87",
    "filename": "Mazzetto et al (2015).xlsx",
    "filepath": "5fcf3fee574d760013166bb6/Mazzetto_et_al_2015/Mazzetto_et_al_2015.xlsx"
  }
  ,

  {
    "_id": "62fa32355bb800001c3b6328",
    "filename": "LCA_data_part_2_v6_updated_ago22.xlsx",
    "filepath": "5fcf3fee574d760013166bb6/Feed_uploads_2/LCA_data_part_2_v6_updated_ago22.xlsx"
  }
];

require('dotenv').config();
const { Lambda } = require('aws-sdk');

const lambda = new Lambda();

const test = (value) => {
  return lambda.invoke({
    FunctionName: 'hestia-files-upload-staging-reindexFile',
    InvocationType: 'Event',
    LogType: 'None',
    Payload: JSON.stringify(value)
  }).promise();
};

Promise.all(values.map(test));
// test(values[0]);
