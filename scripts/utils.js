require('dotenv').config();
const { S3 } = require('aws-sdk');

const s3 = new S3();

const listFolder = async (bucket, prefix = 'aggregation', nextToken) => {
  const { Contents, IsTruncated, NextContinuationToken } = await s3.listObjectsV2({
    Bucket: bucket,
    Prefix: prefix,
    ContinuationToken: nextToken,
    MaxKeys: 1000
  }).promise();
  return [...Contents, ...(IsTruncated ? await listFolder(bucket, prefix, NextContinuationToken) : [])];
};

const deleteObjects = async (bucket, objects, limit = 1000) => {
  const toDelete = objects.slice(0, limit);
  toDelete.length && await s3.deleteObjects({
    Bucket: bucket,
    Delete: {
      Objects: toDelete.map(value => ({ Key: typeof value === 'object' ? value.Key : value }))
    }
  }).promise();
  const remaining = objects.slice(limit);
  return remaining.length ? await deleteObjects(remaining) : true;
};

module.exports = {
  listFolder, deleteObjects
};
