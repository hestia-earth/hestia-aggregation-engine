import os
import json
import numpy as np
from hestia_earth.schema import SchemaType
from hestia_earth.utils.tools import non_empty_list, flatten

# use `FORCE=true pytest ...` to overwirte result file
overwrite_results = os.getenv('FORCE', 'false') == 'true'
fixtures_path = os.path.abspath('tests/fixtures')
start_year = 2000
end_year = 2025
current_date = f"{end_year}-01-01"
TERM = {
    "@type": "Term",
    "@id": "test"
}
WORLD = {'@id': 'region-world', '@type': 'Term'}
SOURCE = {'@id': 'hestia', '@type': 'Source'}
CYCLE = {'@type': 'Cycle', '@id': 'cycle-id'}
PRODUCT_BY_FILENAME = {
    'wheatGrain': {
        "@type": "Term",
        "name": "Wheat, grain",
        "termType": "crop",
        "@id": "wheatGrain",
        "units": "kg"
    },
    'bananaFruit': {
        "@type": "Term",
        "termType": "crop",
        "name": "Banana, fruit",
        "units": "kg",
        "@id": "bananaFruit"
    },
    'cassavaRoot': {
        "@id": "cassavaRoot",
        "@type": "Term",
        "termType": "crop",
        "name": "Cassava, root",
        "units": "kg",
        "defaultProperties": [
            {
                "term": {
                    "@type": "Term",
                    "name": "Dry matter",
                    "termType": "property",
                    "@id": "dryMatter",
                    "units": "%"
                },
                "value": 37.6,
                "@type": "Property"
            }
        ]
    },
    'relative-unit': {
        "@type": "Term",
        "name": "Meat, chicken (liveweight)",
        "termType": "animalProduct",
        "@id": "meatChickenLiveweight",
        "units": "kg liveweight"
    },
    'multiple-dates': {
        "@type": "Term",
        "termType": "crop",
        "name": "Maize, grain",
        "units": "kg",
        "@id": "maizeGrain"
    },
    'management': {
        "@type": "Term",
        "termType": "crop",
        "name": "Banana, fruit",
        "units": "kg",
        "@id": "bananaFruit"
    }
}
TERMS_BY_ID = {
    'organic': {
        '@type': 'Term',
        '@id': 'organic',
        'termType': 'standardsLabels'
    }
}


def fake_aggregated_version(node: dict, *args): return node


def fake_download(id, type=SchemaType.TERM.value, *args):
    return TERMS_BY_ID.get(id) or PRODUCT_BY_FILENAME.get(id) or {'@type': type, '@id': id}


def filter_cycles(values: list): return [c for c in values if c.get('type') == 'Cycle']


properties = [
    'inputs',
    'products',
    'practices',
    'emissions',
    'management',
    'measurements',
    'emissionsResourceUse',
    'impacts',
    'endpoints'
]


def order_lists(obj: dict):
    for prop in properties:
        if prop in obj:
            # use unique keys of the Emission
            obj[prop] = sorted(obj[prop], key=lambda x: ';'.join(non_empty_list(flatten([
                x.get('term', {}).get('@id'),
                x.get('methodModel', {}).get('@id'),
                x.get('transport', {}).get('@id'),
                x.get('operation', {}).get('@id'),
                x.get('transformation', {}).get('@id'),
                [i.get('@id') for i in x.get('inputs', [])],
                [i.get('@id') for i in x.get('animals', [])],
                x.get('dates', []),
                x.get('startDate', []),
                x.get('endDate', []),
                str(x.get('depthUpper', '')),
                str(x.get('depthLower', '')),
            ]))))

    if 'transformations' in obj:
        obj['transformations'] = list(map(lambda t: order_lists(t), obj['transformations']))

    if 'site' in obj:
        obj['site'] = order_lists(obj['site'])

    ordered_keys = sorted(list(obj.keys()))
    return {k: obj.get(k) for k in ordered_keys}


def order_results(results: list): return list(map(order_lists, results))


# fix error "Object of type int64 is not JSON serializable"
class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        if isinstance(obj, np.floating):
            return float(obj)
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return super(NpEncoder, self).default(obj)


def overwrite_expected(filepath: str, data: list):
    if overwrite_results and data:
        with open(filepath, 'w') as outfile:
            json.dump(list(map(order_lists, data)), outfile, indent=2, ensure_ascii=False, cls=NpEncoder)
